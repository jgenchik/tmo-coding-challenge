# Stocks coding challenge

## How to run the application

There are two apps: `stocks` and `stocks-api`.

- `stocks` is the front-end. It uses Angular 7 and Material. You can run this using `yarn serve:stocks`
- `stocks-api` uses Hapi and has a very minimal implementation. You can start the API server with `yarn serve:stocks-api`

A proxy has been set up in `stocks` to proxy calls to `locahost:3333` which is the port that the Hapi server listens on.

> You need to register for a token here: https://iexcloud.io/cloud-login#/register/ Use this token in the `environment.ts` file for the `stocks` app.

> The charting library is the Google charts API: https://developers.google.com/chart/

## Problem statement

[Original problem statement](https://bitbucket.org/kburson3/developer-puzzles/src/3fb1841175cd567a63abfbe18c08e4d2a734c2e9/puzzles/web-api/stock-broker.md)

### Task 1

Please provide a short code review of the base `master` branch:

1. What is done well?
2. What would you change?
3. Are there any code smells or problematic implementations?

> Make a PR to fix at least one of the issues that you identify

### Task 2

```
Business requirement: As a user I should be able to type into
the symbol field and make a valid time-frame selection so that
the graph is refreshed automatically without needing to click a button.
```

_**Make a PR from the branch `feat_stock_typeahead` to `master` and provide a code review on this PR**_

> Add comments to the PR. Focus on all items that you can see - this is a hypothetical example but let's treat it as a critical application. Then present these changes as another commit on the PR.

### Task 3

```
Business requirement: As a user I want to choose custom dates
so that I can view the trends within a specific period of time.
```

_**Implement this feature and make a PR from the branch `feat_custom_dates` to `master`.**_

> Use the material date-picker component

> We need two date-pickers: "from" and "to". The date-pickers should not allow selection of dates after the current day. "to" cannot be before "from" (selecting an invalid range should make both dates the same value)

### Task 4

```
Technical requirement: the server `stocks-api` should be used as a proxy
to make calls. Calls should be cached in memory to avoid querying for the
same data. If a query is not in cache we should call-through to the API.
```

_**Implement the solution and make a PR from the branch `feat_proxy_server` to `master`**_

> It is important to get the implementation working before trying to organize and clean it up.


### My Notes
** What is done well?
The code is written well and unit tests are present. Use of standard Material components is appropriate.
NgRx is used well. I especially appreciate the use of @ngrx/entity. It may be overkill for this project but @ngrx/entity is a very useful package. FYI, @ngrx/data will be incorporated into the next version of NgRx which should make code less verbose.

** What would you change?
I would rate as the first thing to change is the directory structure. I put this ahead of the bug that prevented the chart from being displayed (fixed). I could guess that some components are placed in the "libs" folder so that they can be reused but Angular Elements should have been used instead.
chart.component.html had a bug that pointed to an incorrect data source (fixed).
Form submit button should not be enabled until the form is in the valid state.
I redesigned the way the data is passed between the stocks and chart components. You were passing an Observable. It was subscribed to but never unsubscribed. This is a bad practice that can cause memory leaks. I redesigned using "async" pipe. The pipe will unsubscribe from the Observable.
I would consider a spinner indicating that the chart is being created as it takes some time to render the chart.
In NgRx stock data fetch error is not cleaned up - bug

** Are there any code smells or problematic implementations?
This implementation injects the NGRX Store in a service component. I would inject it in the Component itself (stocks.component), although an argument can be made that it is done to abstract NGRX from the components.
ng test fails.



**************************** Task 3 **************************
Intis task I demonstrate interaction and custom validation of multiple components of a reactive form, not displaying of the charts.
From date and to date are displayed when the "custom" period is selected. Per requirements, they have no default value. User can enter dates in any order.
From date: if "to" date does not exist, the date picker will not go beyond the current date. If "to" date is felt, the date picker will not go beyond the "to" date.
"To" date: the date picker will not go beyond the current date. If it is set to before the "from" date, the "from" date will be adjusted to equal the "to" date.
Custom validator checks that both dates are entered if the period is "custom".

******************** Task 4 *********************
My prefered solution for caching is to create PWA application and configure cache rules for static and dynamic resources.
Ass the data returned from the server is based on the date of request, I cached it based on the date and stock symbol and range.
To remove stale objects from the cache I keep track of the current date. When the date changes,  entities in the cache cannot be relevant so the cache is cleared.